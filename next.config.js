/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'upload.wikimedia.org',
      },
      {
        protocol: 'http',
        hostname: 'uhdtv.io',
      },
      {
        protocol: 'https',
        hostname: 'mango.blender.org',
      },
      {
        protocol: 'https',
        hostname: 'download.blender.org',
      },
      {
        protocol: 'https',
        hostname: 'images.alphacoders.com',
      }
    ],
  },
}

module.exports = nextConfig
